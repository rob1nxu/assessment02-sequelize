(function () {

    var GroceryApp = angular.module("GroceryApp", ["ui.router"]);


    var GroceryConfig = function ($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state("start", {
                url: "/start",
                templateUrl: "views/start.html",
                // controller:"StartCtrl as startCtrl"
            })


            .state("list", {
                url: "/list:product",  // search only product for now
                templateUrl: "views/list.html",
                controller: "ListCtrl as listCtrl"
            })

            .state("brand", {
                url: "/bybrand:brand",
                templateUrl: "views/brand.html",
                controller: "BrandCtrl as brandCtrl"

            })

            .state("edit", {
                url: "/edit",
                templateUrl: "views/edit.html",
                controller: "EditCtrl as editCtrl"
            })

            .state("new", {
                url: "/new/html",
                templateUrl: "views/new.html",
                controller: "NewCtrl as newCtrl"
            })

        $urlRouterProvider.otherwise("/start");

    }

    GroceryConfig.$inject = ["$stateProvider", "$urlRouterProvider"];




    var GrocerySvc = function ($http, $q) {

        var grocerySvc = this;

        grocerySvc.editItem = "";
        // find by productname


        grocerySvc.byProduct = function (name) {
            var defer = $q.defer();
            $http.get("/api/product/" + name)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject();
                });
            return (defer.promise);

        }

        grocerySvc.byBrand = function (brand) {
            var defer = $q.defer();
            $http.get("/api/brand/" + brand)
                .then(function (result) {
                    defer.resolve(result.data);
                }).catch(function (err) {
                    defer.reject(err);
                });
            return (defer.promise);
        };


        grocerySvc.checkUPC = function (upc) {
            var defer = $q.defer();
            $http.get("/api/checkUpc/" + upc)
                .then(function (result) {
                    defer.resolve(result.data);
                }).catch(function (err) {
                    defer.reject(err);
                });
            return (defer.promise);
        };




    }
    GrocerySvc.$inject = ["$http", "$q"];



    var ListCtrl = function ($state, GrocerySvc, $stateParams) {

        var listCtrl = this;

        listCtrl.getProduct = "";


        listCtrl.productName = $stateParams.product;
        console.log("search product: %s", listCtrl.productName);

        listCtrl.brandName = $stateParams.brand;
        console.log("search brand: %s", listCtrl.brandName);

        GrocerySvc.byProduct($stateParams.product)

            .then(function (result) {
                console.log("this is result at GrocerySvc.byProduct: $s", result);

                listCtrl.getProduct = result;

                // reviewCtrl.details = result.data.book.critic_reviews;
                // console.log("this is reviewCtrl.details.book.critic_reviews in $s", reviewCtrl.details);

            })
            .catch(function (err) {
                console.error("err = ", err)
            });



        listCtrl.edit = function (select) {

            listCtrl.toedit = select;
            console.log("select is %s", select);

            listCtrl.selectedItem = listCtrl.getProduct[listCtrl.toedit];
            console.log("user is selecting $s", listCtrl.selectedItem);

            GrocerySvc.editItem = listCtrl.selectedItem;

            $state.go("edit");

        }



    }

    ListCtrl.$inject = ["$state", "GrocerySvc", "$stateParams"];











    var MainCtrl = function ($state, GrocerySvc) {

        var mainCtrl = this;


        mainCtrl.searchProduct = "";

        mainCtrl.searchBrand = "";

        mainCtrl.Psearch = function () {
            console.info(">>> product name = %s", mainCtrl.searchProduct);
            $state.go("list", { product: mainCtrl.searchProduct });
            mainCtrl.searchProduct = "";
        }

        mainCtrl.Bsearch = function () {
            console.info(">>> brand name = %s", mainCtrl.searchBrand);
            $state.go("brand", { brand: mainCtrl.searchBrand });
            mainCtrl.searchBrand = "";
        }


        mainCtrl.new = function () {

            $state.go("new");

        }

    }
    MainCtrl.$inject = ["$state", "GrocerySvc"];



    var NewCtrl = function ($state, GrocerySvc, $http, $q) {

        var newCtrl = this;
        // var existingUPC = true;
        newCtrl.productname = "";
        newCtrl.brandname = "";
        newCtrl.upc = "";
        newCtrl.showStatus = false;

        newCtrl.cx = function () {
            $state.go("start");
        }

        newCtrl.add = function () {



            console.log("submitted new productname is: %s", newCtrl.productname);
            console.log("submitted new brandname is: %s", newCtrl.brandname);
            console.log("submitted new upc is: %s", newCtrl.upc);


            GrocerySvc.checkUPC(newCtrl.upc)
                .then(function (result) {
                    console.log("result is %s", result);

                    if (result == false) { //have to use ==, not just one = because boolean is a type not just a value like an integer

                        var defer = $q.defer();

                    $http.post("/api/add", {
                        name: newCtrl.productname,
                        brand: newCtrl.brandname,
                        upc12: newCtrl.upc
                    })

                        .then(function (result) {
                            defer.resolve(result);


                            console.log("successfully added");

                            newCtrl.showStatus = result;




                        }).catch(function (err) {
                            defer.reject(err);

                        })
                    return (defer.promise);
                }
                
                else{
                    console.log("upc number is not unique");
                }

            

                }).catch(function (err) {
                    console.log("upc no. not unique or server error");

                })





        }

    }
    NewCtrl.$inject = ["$state", "GrocerySvc", "$http", "$q"];










    var BrandCtrl = function ($state, GrocerySvc, $stateParams) {

        var brandCtrl = this;

        brandCtrl.brandname = $stateParams.brand;
        brandCtrl.getBrand = "";


        brandCtrl.edit = function (select) {

            brandCtrl.toedit = select;
            console.log("select is %s", select);

            brandCtrl.selectedItem = brandCtrl.getBrand[brandCtrl.toedit];
            console.log("@brand user is selecting $s", brandCtrl.selectedItem);

            GrocerySvc.editItem = brandCtrl.selectedItem;

            $state.go("edit");

        }



        GrocerySvc.byBrand($stateParams.brand)

            .then(function (result) {
                console.log("this is result at GrocerySvc.byBrand: $s", result);

                brandCtrl.getBrand = result;

                // reviewCtrl.details = result.data.book.critic_reviews;
                // console.log("this is reviewCtrl.details.book.critic_reviews in $s", reviewCtrl.details);

            })
            .catch(function (err) {
                console.error("err = ", err)
            });





    }

    BrandCtrl.$inject = ["$state", "GrocerySvc", "$stateParams"];



    var EditCtrl = function ($state, GrocerySvc, $http, $q) {

        var editCtrl = this;

        editCtrl.showStatus = false;

        editCtrl.ToBeEdited = GrocerySvc.editItem;
        console.log("we are editing this: $s", editCtrl.ToBeEdited);

        editCtrl.cx = function () {
            $state.go("start");

        }



        editCtrl.save = function () {

            console.log("productname to be edited is: %s", editCtrl.ToBeEdited.name);
            console.log("brandname to be edited is: %s", editCtrl.ToBeEdited.brand);
            console.log("id of item being edited: %$s", editCtrl.ToBeEdited.id)
            // var defer = $q.defer();

            var defer = $q.defer();

            $http.post("/api/edit", {
                name: editCtrl.ToBeEdited.name,
                brand: editCtrl.ToBeEdited.brand,
                id: editCtrl.ToBeEdited.id

            })

                .then(function (result) {
                    defer.resolve(result);


                    console.log("successfully edited");

                    editCtrl.showStatus = result;




                }).catch(function (err) {
                    defer.reject(err);

                })
            return (defer.promise);


        }




    }

    EditCtrl.$inject = ["$state", "GrocerySvc", "$http", "$q"];








    GroceryApp.config(GroceryConfig);

    GroceryApp.service("GrocerySvc", GrocerySvc);

    GroceryApp.controller("MainCtrl", MainCtrl);
    GroceryApp.controller("ListCtrl", ListCtrl);
    GroceryApp.controller("BrandCtrl", BrandCtrl);
    GroceryApp.controller("EditCtrl", EditCtrl);
    GroceryApp.controller("NewCtrl", NewCtrl);

})();


