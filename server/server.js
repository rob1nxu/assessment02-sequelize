const uuid = require("uuid/v4");
const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const Sequelize = require("sequelize");




const MYSQL_USERNAME="a2";
const MYSQL_PASSWORD="Password1234";

var connection = new Sequelize(
    'assessment2',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host:'localhost',
        logging: console.log,
        dialect: 'mysql',
        
        pool:{
            max: 5,
            min: 0,
            idle: 10000
        }

    }
);

//console.log(connection); // use to check the connection


var Groceries = require("./models/grocery_list")(connection, Sequelize);

const app = express();



app.use(bodyParser.urlencoded({extended: false })); //need to set size limit for upload
app.use(bodyParser.json());






app.get("/api/product/:name", function (req, res) {



    const nameProduct = req.params.name;
    console.log("nameProduct is %s", nameProduct);


	Groceries.findAll({
		where: {name:{$like: "%" + nameProduct + "%"}},
		limit: 20,
		order: [
			['name', 'ASC']
		]

	}).then(function(results){
		if (results.length > 0) {
				res.status(200);
                res.json(results);
			}
		// console.log(results);

	}).catch(function(err){
		console.log("rejected");
	 		res.status(404);
			res.end();
	});



});



 app.get("/api/brand/:brand", function (req, res) {



 const nameBrand = req.params.brand;
    console.log("nameBrand is %s", nameBrand);

	Groceries.findAll({
		where: {brand:{$like: "%" + nameBrand + "%" } },
		limit: 20,
		order:[
			['brand', 'ASC']
		]
	}).then(function(brand){

		if (brand.length > 0) {
				res.status(200);
                res.json(brand);
			}

	}).catch(function(err){

		console.log("rejected");
		res.status(404);
		res.end();

	})


 });





//INSERT INTO `grocery_list` (`id`,`upc12`,`brand`,
//`name`) VALUES (DEFAULT,'987654233','samsung','note8');
 
 app.post("/api/add", function(req, res){


	 
	console.info(" product name is: %s", req.body.name);
     console.info(" brand name is: %s", req.body.brand);
     console.info("upc12 number is %s", req.body.upc12)

	 var newProduct = {
		 upc12: req.body.upc12,
		 brand: req.body.brand,
		 name: req.body.name
	 }


	 Groceries.create(newProduct)
	 .then(function(result){
		console.log("new product added!");
		var successMsg = true;
		res.status(200).json(successMsg);

	 }).catch(function(err){
		console.log("adding product failed");


	 })

  });




 app.post("/api/edit", function(req, res){


	 
  	console.info(" product name is: %s", req.body.name);
     console.info(" brand name is: %s", req.body.brand);
     console.info(" id is %s", req.body.id)
 	console.info(" type of id is %s", typeof(req.body.id));



	 Groceries.update(
		 {brand: req.body.brand, name: req.body.name},
		// {name:req.body.name},
		 {where: {id: req.body.id}}

	)
	.then(function(result){
		console.log("successfully edited");
		var successMsg = true;
		res.status(200).json(successMsg);


	}).catch(function(err){
		console.log("editing product failed");


	})
	 

  });



app.get("/api/checkUpc/:upc", function(req, res){

	console.log("checking following upc no. %s", req.params.upc);

	Groceries.count({
		where: {upc12: req.params.upc},
		limit: 20
	}).then(function(results){

		console.log("here is the count: %s",results);

		if(results!= 0) 
		{
		var signal = true;
		res.status(200).json(signal);
		}

		else
		{
		var signal = false;
		res.status(200).json(signal);
		}

	}).catch(function(err){
		console.log("error");
	})

})








app.use("/libs", express.static(path.join(__dirname, "../bower_components")));

app.use(express.static(path.join(__dirname, "../client")));


const port = 3005;

app.listen(port, function(){
    console.info("Application started on port %d", port);
	console.info("time stamp: %s", new Date());
    // console.log("\tsession key = %s", key);
})