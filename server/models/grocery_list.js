module.exports = function(conn, Sequelize){

var grocerylist = conn.define("groceries",
{
        id:{
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        upc12:{
            type: Sequelize.INTEGER(12),
            allowNull: false

        },

        brand:{
            type: Sequelize.STRING,
            allowNull: false
        },

        name:{
            type: Sequelize.STRING,
            allowNull: false
        }



},

    {
        tableName: "grocery_list",
        timestamps: false


    }



);


return grocerylist;


}